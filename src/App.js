import React, { useState } from "react";
import './App.css';
import Header from './common/header';
import Login from './routes/Login/Login';
import Register from './routes/Register/Register';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import { AuthContext } from "./context/auth";
import User from "./routes/Dashboard/User";
import Admin from "./routes/Dashboard/Admin";

function App() {
  const [isAuthenticated] = useState();
  const initialToken = localStorage.getItem("tokens");
  const [authTokens, setAuthTokens] = useState(initialToken);
  const [pageTitle, setPageTitle] = useState("Basma");

  const setTokens = (data) => {
    localStorage.setItem("tokens", data);
    setAuthTokens(data);
  }
  const logout = () => {
    setTokens(null);
  };
  const changetitle = (new_title) => {
    setPageTitle(new_title);
  };

  
  return (
    <AuthContext.Provider value={{ authTokens, setAuthTokens: setTokens }}>
      <Router>
        <div className="App">
          <Header title={pageTitle} logout={logout} />
          <Switch>
            <Route path="/" exact>
              {isAuthenticated ? (
                <Redirect to="/dashboard" />
              ) : (
                <Login 
                changetitle={(title) => changetitle(title)}/>
              )}
            </Route>
            <Route path="/login">
              <Login
              changetitle={(title) => changetitle(title)}
              />
            </Route>
            <Route path="/register">
              <Register
              changetitle={(title) => changetitle(title)}
              />
            </Route>
            <Route path="/admin">
              <Admin
              changetitle={(title) => changetitle(title)}
            />
            </Route>
            <Route path="/user">
              <User
              changetitle={(title) => changetitle(title)}
            />
            </Route>
          </Switch>
        </div>
      </Router>
    </AuthContext.Provider>
  );
}
export default App;
