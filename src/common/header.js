// Importing combination
import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    menuButton: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
}));




export default function Header(props) {
    const classes = useStyles();
    const initialToken = localStorage.getItem("tokens");
    let buttons;
    if((initialToken && initialToken === 'null') || !initialToken){
        buttons = <div><Button color="inherit" href="login">Login</Button><Button color="inherit" href="register">Register</Button></div>
    }else{
        buttons = <div><Button color="inherit" onClick={props.logout}>Logout</Button></div>
    };
  
    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                <Typography variant="h6" className={classes.title}>{props.title}</Typography>
                {buttons}
                </Toolbar>
            </AppBar>
        </div>
    )
}

