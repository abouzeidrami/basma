import React , { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useAuth } from "../../context/auth";
import { do_login } from "../../services/api";
import { useSnackbar } from 'notistack';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import {
  Redirect,
} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function Login({ changetitle, ...rest }) {
  const { enqueueSnackbar } = useSnackbar();
  const [redirecturl, setRedirecturl] = useState('');
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const { setAuthTokens } = useAuth();

  useEffect(() => {
    changetitle('Login');
  }, [changetitle]);

  const login = () => {
    var params = {
      'email': email,
      'password': password
    };
    do_login(params).then(function(result){
      setAuthTokens(result.access_token);
      var message_to_display = "Successfully logged in as " + result.user.name;
      if(result.user.role === 1){
        message_to_display += " (Super Admin)"
        setRedirecturl('/admin');
      }else{
        message_to_display += " (User)"
        setRedirecturl('/user');
      }
      enqueueSnackbar(message_to_display);
    }, function(result){
        console.log(result);
    });
	};
  if(redirecturl !== ''){
    return (
      <Redirect to={redirecturl} />
    )
  }
  return (
    <Container maxWidth="sm">
      <form className={classes.root} noValidate autoComplete="off">
        <FormControl fullWidth>
        <TextField 
          id="email" 
          label="Email" 
          value={email}
          onChange={e => {
            setEmail(e.target.value);
          }} 
        />
        </FormControl>
        <FormControl fullWidth>
        <TextField 
          id="password" 
          type="password"
          label="Password" 
          value={password}
          onChange={e => {
            setPassword(e.target.value);
          }} 
        />
        </FormControl>
        <FormControl fullWidth>
        <Button onClick={() => login()}>Login</Button>
        </FormControl>
      </form>
    </Container>
  );
}