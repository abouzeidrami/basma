import React, { useState, useEffect } from 'react';
import { fetch_customers } from "../../../services/api";
import { DataGrid } from '@material-ui/data-grid';
import { useSnackbar } from 'notistack';

const columns = [
    { field: 'id', headerName: 'Id', flex: 1 },
    { field: 'name', headerName: 'Name', flex: 1 },
    { field: 'email', headerName: 'Email', flex: 1 },
    { field: 'created_at', headerName: 'Date created', flex: 1},
];

export default function Customers() {
    const [rows, setRows] = useState([]);


    const { enqueueSnackbar } = useSnackbar();
    useEffect(() => {
        fetch_customers().then(function(result){
            setRows(result.users);
            enqueueSnackbar("Customers loaded successfully");
        }, function(result){
            enqueueSnackbar(result.message);
        });
    }, [enqueueSnackbar]);

    if(rows.length > 0){
        return (
            <div>
                <h1>Check Customers list</h1>
                <DataGrid rows={rows} columns={columns} autoHeight />
            </div>
        )
    }else{
        return (
            <div>No data</div>
        );
    }
}