import React, { useState } from "react";
import { DateRangePicker, DateRange } from "materialui-daterange-picker";
import TextField from '@material-ui/core/TextField';
import Moment from 'react-moment';
import 'moment-timezone';
import moment from 'moment';
import { get_average } from "../../../services/api";
import { Chart } from 'react-charts'


 
export default function Graph() {
  
  const [open, setOpen] = useState(true);
  const [average, setAverage] = useState([]);
  const [dateRange, setDateRange] = useState(
    {startDate: moment().format('YYYY-MM-DD'), 
    endDate:moment().add(-7, 'd').format('YYYY-MM-DD'), 
    display: ''
  });
 
  const call_average = (startDate, endDate) => {
    var params = {
      "start_date": startDate,
      "end_date": endDate
    }
    get_average(params).then(function(result){
      if(result.count.length>0){
        //setAverage({"data":result.count})
      }
    }, function(result){
    });
  }

  const axes = [
    { primary: true, type: 'time', position: 'bottom' },
    { type: 'linear', position: 'left' }
  ]

  const toggle = () => setOpen(!open);
 
  const changeDateRange = (range) => {
    // range.display = moment(range.startDate).format('YYYY-MM-DD') + ' - ' + moment(range.endDate).format('YYYY-MM-DD')
    // toggle();
    // call_average(moment(range.startDate).format('YYYY-MM-DD'), moment(range.endDate).format('YYYY-MM-DD'));
    // setDateRange(range);
  }

  return (
    <div>
      <TextField 
        id="daterange" 
        label="Date" 
        value={dateRange.display}
        onClick={toggle}
      />
      <DateRangePicker
        open={open}
        toggle={toggle}
        onChange={(range) => changeDateRange(range)}
      />

      <Chart data={average} axes={axes} tooltip />
    </div>
  );
}
