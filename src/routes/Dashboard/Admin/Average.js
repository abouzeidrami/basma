import React, { useState } from "react";
import { DateRangePicker } from "materialui-daterange-picker";
import TextField from '@material-ui/core/TextField';
import 'moment-timezone';
import moment from 'moment';
import { get_average } from "../../../services/api";


 
export default function Average() {
  
  const [open, setOpen] = useState(true);
  const [average, setAverage] = useState([]);
  const [dateRange, setDateRange] = useState(
    {startDate: moment().format('YYYY-MM-DD'), 
    endDate:moment().add(-7, 'd').format('YYYY-MM-DD'), 
    display: ''
  });
 
  const call_average = (startDate, endDate) => {
    var params = {
      "start_date": startDate,
      "end_date": endDate
    }
    get_average(params).then(function(result){
      if(result.count.length>0){
        setAverage(result.count)
      }
    }, function(result){
    });
  }
  const toggle = () => setOpen(!open);
 
  const changeDateRange = (range) => {
    range.display = moment(range.startDate).format('YYYY-MM-DD') + ' - ' + moment(range.endDate).format('YYYY-MM-DD')
    toggle();
    setDateRange(range);
    call_average(moment(range.startDate).format('YYYY-MM-DD'), moment(range.endDate).format('YYYY-MM-DD'));
  }

  const listItems = average.map((number) =>
    <li>{number.count} new users on {number.date} </li>
  );


  return (
    <div>
    <h1>Check average number of registrations</h1>
      <TextField 
        id="daterange" 
        label="Date" 
        value={dateRange.display}
        onClick={toggle}
      />
      <DateRangePicker
        open={open}
        toggle={toggle}
        onChange={(range) => changeDateRange(range)}
      />
      {listItems}
    </div>
  );
}
