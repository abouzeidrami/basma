import React, { useState, useEffect } from "react";
import { user_profile } from "../../services/api";
import {
  Redirect
} from "react-router-dom";


function User({ changetitle, logout, ...rest }) {
  const [user, setUser] = useState({name:'', email:'', created_at:''})
  useEffect(() => {
    changetitle('User');
    user_profile().then(function(result){
      result.created_at = (new Date(result.created_at)).toLocaleDateString();
      setUser(result);
    }, function(result){
        console.log(result);
    });
  }, [changetitle]);

  const initialToken = localStorage.getItem("tokens");
  if((initialToken && initialToken === 'null') || !initialToken){
    return (
      <Redirect to='/login' />
    )
  }

  return (
    <div>
      <h1>Welcome {user.name}, Your email is: {user.email}</h1>
      <h2>Your account has been created on {user.created_at}</h2>
    </div>
  );
}

export default User;