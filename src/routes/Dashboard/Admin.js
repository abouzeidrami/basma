import React, { useEffect } from "react";
import Average from "./Admin/Average";
import Customers from "./Admin/Customers";
import Container from '@material-ui/core/Container';
import {
  Redirect
} from "react-router-dom";


function Admin({ logout, changetitle, ...rest }) {

  useEffect(() => {
    changetitle('Admin');
  }, [changetitle]);

  const initialToken = localStorage.getItem("tokens");
  if((initialToken && initialToken === 'null') || !initialToken){
    return (
      <Redirect to='/login' />
    )
  }

  return (
    <Container maxWidth="lg">
    <div>
			<Customers />
      <Average />
    </div>
    </Container>
  );
}

export default Admin;