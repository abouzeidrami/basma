import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useAuth } from "../../context/auth";
import { do_register, do_login } from "../../services/api";
import { useSnackbar } from 'notistack';
import Container from '@material-ui/core/Container';
import FormControl from '@material-ui/core/FormControl';
import {
  Redirect,
} from "react-router-dom";


const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
}));

export default function Register({ changetitle, ...rest }) {
  const { enqueueSnackbar } = useSnackbar();
  const [redirecturl, setRedirecturl] = useState('');
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [passwordconfirm, setPasswordconfirm] = useState("");
  const { setAuthTokens } = useAuth();
  const [isPassMatching, setIsPassMatching] = useState(false);
  const classes = useStyles();
  useEffect(() => {
    changetitle('Register');
  }, [changetitle]);
	const register = () => {
    var params = {
      'name': name,
      'email': email,
      'password': password,
      'password_confirmation': passwordconfirm
    };
    do_register(params).then(function(result){
      do_login(params).then(function(result){
        setAuthTokens(result.access_token);
        var message_to_display = "Successfully logged in as " + result.user.name;
        if(result.user.role === 1){
          message_to_display += " (Super Admin)"
          setRedirecturl('/admin');
        }else{
          message_to_display += " (User)"
          setRedirecturl('/user');
        }
        enqueueSnackbar(message_to_display);
      }, function(result){
          console.log(result);
      });
      enqueueSnackbar(result.message);
    }, function(result){
      enqueueSnackbar("An error occured");
    });
	};


  const changePassword = (e) => {
    setPassword(e.target.value);
    setIsPassMatching(password === passwordconfirm);
  }

  const changePasswordConfirm = (e) => {
    setPasswordconfirm(e.target.value);
    setIsPassMatching(passwordconfirm === password);
  }
  if(redirecturl !== ''){
    return (
      <Redirect to={redirecturl} />
    )
  }

  return (
    <Container maxWidth="sm">
    <form className={classes.root} noValidate autoComplete="off">
      <FormControl fullWidth>
      <TextField 
        id="name" 
        label="Name" 
        value={name}
        onChange={e => {
          setName(e.target.value);
        }} 
      />
      <TextField 
        id="email" 
        label="Email" 
        value={email}
        onChange={e => {
          setEmail(e.target.value);
        }} 
      />
      </FormControl>
      <FormControl fullWidth>
      <TextField 
        id="password" 
        label="Password" 
        type="password"
        value={password}
        onChange={e => {
          changePassword(e);
        }} 
      />
      <TextField 
        id="passwordconfirm" 
        label="Password Confirmation" 
        type="password"
        error={isPassMatching}
        value={passwordconfirm}
        onChange={e => {
          changePasswordConfirm(e);
        }} 
      />
      </FormControl>
      <FormControl fullWidth>
      <Button onClick={() => register()}>Register</Button>
      </FormControl>
    </form>
    </Container>
  );
}