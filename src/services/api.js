import axios from "axios";


const http_call = (options) => {
    // Options
    var method = options.method;
    var endpoint = options.endpoint;
    var params = options.params;
    // Fill the data
    //var domain = "";
    var endpoint_url = endpoint;
    console.log(endpoint);
    //return false;
    var url = endpoint_url;
    var json_params = {
        'format': 'json'
    };
    var headers = {
        'Content-Type': 'application/x-www-form-urlencoded'
    };
    var initialToken = localStorage.getItem('tokens');
    if (options.require_token === 1) {
        headers['Authorization'] = 'Bearer ' + initialToken;
    }
    //if(typeof app_token != 'undefined'){
    //json_params['app_access_token'] = get_session_data('app_access_token');
    //}
    if (typeof params != 'undefined') {
        if (Object.keys(params).length > 0) {
            for (var i = 0; i < params.length; i++) {
                json_params[i] = params[i];
            }
            ;
            // angular.forEach(params, function(value, key){
            //   json_params[key] = value;
            // });
        }
    }
    if (method === 'post' || method === 'get') {
        json_params['timestamp'] = new Date().getTime();
    }
    // if(language_request !=''){
    //     json_params.lang=language_request;
    // }
    var $httpOptions = {
        method: method,
        url: url,
        params: params,
        headers: headers
    };
    if (method === 'GET') {
        $httpOptions.data = params;
    }
    var promise = new Promise((resolve, reject) => {
        axios($httpOptions)
            .then(response => {
                resolve(response.data);
            })
            .catch(error => {
                reject(error);
            });
    });
    return promise;
}

function do_login(params){
    var endpoint = 'api/auth/login';
    var options = { 'method': 'POST', 'params': params, 'endpoint': endpoint };
    return http_call(options);
};

function do_register(params){
    var endpoint = 'api/auth/register';
    var options = { 'method': 'POST', 'params': params, 'endpoint': endpoint };
    return http_call(options);
};

function user_profile(){
    var endpoint = 'api/auth/user-profile';
    var options = { 'method': 'GET', 'endpoint': endpoint, 'require_token': 1 };
    return http_call(options);
}

function fetch_customers(){
    var endpoint = 'api/customers/list';
    var options = { 'method': 'POST', 'endpoint': endpoint, 'require_token': 1 };
    return http_call(options);
}

function get_average(params){
    var endpoint = 'api/customers/average';
    var options = { 'method': 'GET', 'params': params, 'endpoint': endpoint, 'require_token': 1 };
    return http_call(options);
}
export {
    do_login,
    do_register,
    user_profile,
    fetch_customers,
    get_average
};
